To build the project using first Beaver and then Cup2:

ant build-all

This generates "BeaverToCup2.jar" It translates .beaver files to CUP2Spec.java

To run it use:

java -jar BeaverToCup2.jar inFile=somefile.beaver outDir=. defaultTokenType=beaver.Symbol

To specify which parser the program should use, there is an optional parameter with 2 possible values:

parserClass=lang.parser.BeaverParser
parserClass=lang.parser.CUP2Parser

Build process:
clean : delete everything in ant-bin and src/gen.
beaver : this builds the beaver parser and beaver compatible scanner
jar : this first jar can only use beaver.
cup2 : this uses the first jar generated above to translate itself to cup2
jar : this second jar can use beaver and cup2.