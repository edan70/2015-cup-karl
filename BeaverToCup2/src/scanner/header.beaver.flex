package lang.scanner;

import static lang.parser.BeaverSpec.Terminals.*; // The terminals are implicitly defined in the parser

%%

// define the signature for the generated scanner
%public
%final
%class BeaverScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol
%function nextToken

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{
	private beaver.Symbol token(short id, String text) {
		return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), text);
	}
  
	private beaver.Symbol token(short id) {
		return token(id, yytext());
	}
%}

