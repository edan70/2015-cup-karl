
// macros
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment} | {DocumentationComment}

TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
// Comment can be the last line of the file, without line terminator.
EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}?
DocumentationComment = "/**" {CommentContent} "*"+ "/"
CommentContent       = ( [^*] | \*+ [^/*] )*

ID = [a-zA-Z0-9_]+

%%

// discard whitespace information
{WhiteSpace}		{ }
{Comment}			{ }

// token definitions
"%package"			{ return token(PACKAGE); }
"%class"			{ return token(CLASS); }
"%embed"			{ return token(EMBED); }
"%typeof"			{ return token(TYPEOF); }
"%goal"				{ return token(GOAL); }
"%terminals"		{ return token(TERMINALS); }
"%header"			{ return token(HEADER); }
"%import"			{ return token(IMPORT); }
"%left"				{ return token(LEFT); }
"%right"			{ return token(RIGHT); }
"%nonassoc"			{ return token(NONASSOC); }

//constants
{ID}				{ return token(ID, yytext()); }

//between tokens
"{:" ~":}"			{ return token(COLONBRACKETS, yytext()); }
"\"" ~"\""			{ return token(STRING, yytext()); }

//characters
"="					{ return token(EQUALS); }
"|"					{ return token(ALTERNATE); }
"\."				{ return token(PERIOD); }
";"					{ return token(SEMICOLON); }
","					{ return token(COMMA); }
"@"					{ return token(AT); }

/* error fallback */
<<EOF>>				{ return token(EOF); }
[^]					{ throw new RuntimeException("Illegal character <"+yytext()+">"); }
