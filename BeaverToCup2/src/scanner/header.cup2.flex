package lang.scanner;

import static lang.parser.CUP2Spec.Terminals.*;

%%
%public
%final
%class CUP2Scanner
%cup2//adds cup2 compatibility. call token(TERM) or token(TERM, yytext()) to create token without/with value.


%{
	public static Terminal EOF=SpecialTerminals.EndOfInputStream;
%}