package lang;

import java.io.*;
import java.nio.file.*;

public class ExtendJPreProcessor{

	public static String readFile(String filename) throws IOException{
    	return new String(Files.readAllBytes(Paths.get(filename)));
	}

	public static void preprocess(String in, String out) throws IOException{
		String input=readFile(in);
		
		//Delete old header and embed sections
		input=input.replaceAll("(?s)%(header|embed|import).*?\\:};", "");

		//fix a type cast. beaver.Symbol to String. This is the only way I could think of that makes sense.
		input=input.replaceAll("return DOCUMENTATION_COMMENT;", "return (String) (DOCUMENTATION_COMMENT.value);");
		input=input.replace("return new Symbol(Terminals.DOCUMENTATION_COMMENT, ((String)dc.value));", "return (String)dc.value;");
		input=input.replace("return new Symbol(Terminals.DOCUMENTATION_COMMENT, dc);", "return dc;");

		
		PrintWriter pw=new PrintWriter(new OutputStreamWriter(new FileOutputStream(out)));
		pw.println("%package \"org.extendj.parser\";");
		pw.println("%import \"org.extendj.ast.*\";");
		pw.println("%terminals POSTINCDEC, UNARY, CAST, BOTTOM;");
		pw.println(input);
		pw.close();
	}
}