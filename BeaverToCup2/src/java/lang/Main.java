package lang;

import java.io.*;
import java.util.*;

import lang.ast.*;
import lang.parser.ParserFactory;
import lang.parser.ParserInterface;

import java.nio.file.*;

public class Main {
	private static Map<String, String> map;

	public static String param(String key) {
		return map.get(""+key);
	}

	public static void processParams(String[] args) {
		map = new HashMap<>();
		map.put("defaultTokenType", "String");
		map.put("defaultClassName", "Parser");
		map.put("outDir", ".");
		map.put("extendj", "false");
		for (String arg : args) {
			if (arg.contains("=")) {
				map.put(arg.substring(0, arg.indexOf('=')), arg.substring(arg.indexOf('=') + 1));
			}
			else{
				map.put(arg, "true");
			}
		}
	}

	public static String readFile(String filename) throws IOException {
		return new String(Files.readAllBytes(Paths.get(filename)));
	}

	public static void printUsage(){
		System.out.println("java -jar BeaverToCUP2.jar [options] inFile=file.beaver");
		System.out.println();
		System.out.println(" [options] is optional, and could contain any of the following:");
		System.out.println(" If a parameter value contains spaces, please use \"\" around the value.");
		System.out.println();
		System.out.println(" defaultTokenType=String");
		System.out.println("  Sets tokens with undefined types to hold values of type String. (default)");
		System.out.println();
		System.out.println(" defaultClassName=Parser");
		System.out.println("  The program will generate Parser.java. (default) (used if no %class \"...\"; exists)");
		System.out.println();
		System.out.println(" outDir=.");
		System.out.println("  Place output file in ./ directory.  (default)");
		System.out.println();
		System.out.println(" extendj=true");
		System.out.println("  Run extendj preprocessor. (not default) Makes scripted edits for extendj.");
		System.out.println();
		System.out.println("To use BeaverToCUP2 with extendj, please use:");
		System.out.println("inFile=src/tmp/parser/JavaParser.beaver extendj=true outDir=src/gen/org/extendj/parser defaultTokenType=beaver.Symbol");
		
	}
	/**
	 * Entry point
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// process parameters
		processParams(args);
		
		String filename = param("inFile");
		if (filename == null) {
			System.out.println("parameter inFile required!");
			printUsage();
			System.exit(-1);
		}

		// possibly run extendj preprocessor
		if (param("extendj").equals("true")) {
			ExtendJPreProcessor.preprocess(filename, "preprocessed.beaver");
			filename = "preprocessed.beaver";
		}

		// possibly change parser, then get a parser instance
		if (param("parserClass") != null) {
			ParserFactory.defaultParserClassName = param("parserClass");
		}
		ParserInterface parser = ParserFactory.instantiateParser();

		// parse the file
		BeaverFile beaverFile = (BeaverFile) parser.parse(filename);

		// set parameters needed and generate output.
		beaverFile.defaultTokenType = param("defaultTokenType");
		beaverFile.defaultClassName = param("defaultClassName");
		PrintStream pw;
		String outFilename = param("outDir") + "/" + beaverFile.specName() + ".java";
		pw = createFile(outFilename);
		if (param("cup") == null) {
			beaverFile.generateParserSpec(0, pw);
		} else {
			beaverFile.CUP(pw);
		}
		pw.close();

		System.out.println("Generated file " + outFilename);

	}

	public static PrintStream createFile(String path) throws Exception {
		File file = new File(path.substring(0, path.lastIndexOf('/')));
		file.mkdirs();
		return new PrintStream(new FileOutputStream(path));
	}
}
