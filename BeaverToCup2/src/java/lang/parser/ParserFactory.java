package lang.parser;
public class ParserFactory{
    public static String defaultParserClassName="lang.parser.BeaverParser";

    public static ParserInterface instantiateParser(String parserClassName) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        
    	Class<?> parserClass = Class.forName(parserClassName);
        return (ParserInterface) parserClass.newInstance();
    }

    public static ParserInterface instantiateParser() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
    	return instantiateParser(defaultParserClassName);
    }
}