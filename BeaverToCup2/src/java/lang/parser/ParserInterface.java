package lang.parser;

import java.io.IOException;

public interface ParserInterface {
	public Object parse(String filename) throws ParserException, IOException;
}
