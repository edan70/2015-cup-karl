package lang.parser;

public class ParserException extends Exception {

	public ParserException(String string) {
		super(string);
	}

}
