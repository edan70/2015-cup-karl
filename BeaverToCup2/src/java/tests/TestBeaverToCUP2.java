package tests;

import java.io.FileOutputStream;
import java.io.PrintStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import lang.ast.BeaverFile;

/**
 * Tests AST dumping
 * modified by Karl Rikte 2016-01-10
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
@RunWith(Parameterized.class)
public class TestBeaverToCUP2 extends AbstractParameterizedTest {
	/**
	 * Directory where test files live
	 */
	private static final String TEST_DIR = "testfiles/";

	/**
	 * Construct a new JastAdd test
	 * @param testFile filename of test input file
	 */
	public TestBeaverToCUP2(String testFile) {
		super(TEST_DIR, testFile);
	}

	/**
	 * Run the JastAdd test
	 */
	@Test
	public void runTest() throws Exception {
		BeaverFile bf = (BeaverFile) parse(inFile);
		PrintStream ps=new PrintStream(new FileOutputStream(outFile));
		bf.generateParserSpec(0, ps);
		ps.close();
		compareOutput(outFile, expectedFile);
	}

	@SuppressWarnings("javadoc")
	@Parameters(name = "{0}")
	public static Iterable<Object[]> getTests() {
		return getTestParameters(TEST_DIR);
	}
}
