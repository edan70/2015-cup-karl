package lang.parser;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import lang.parser.BeaverSpec;
import lang.scanner.BeaverScanner;

public class BeaverParser implements ParserInterface{
	BeaverSpec bp=new BeaverSpec();
	
	@Override
	public Object parse(String filename) throws ParserException, IOException {
		try{
			beaver.Scanner scanner = new BeaverScanner(new InputStreamReader(new FileInputStream(filename)));
			return bp.parse(scanner);
		}catch(beaver.Parser.Exception e){
			throw new ParserException("beaver.Parser.Exception: "+e.getMessage());
		}
	}
}
