package lang.parser;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import edu.tum.cup2.generator.LR1Generator;
import edu.tum.cup2.parser.LRParser;
import edu.tum.cup2.parser.exceptions.LRParserException;
import edu.tum.cup2.parser.tables.LRParsingTable;
import lang.scanner.CUP2Scanner;

public class CUP2Parser implements ParserInterface {
	LRParser parser;

	public CUP2Parser() throws Exception{
		LRParsingTable table = new LR1Generator(new CUP2Spec()).getParsingTable();
		parser = new LRParser(table);
	}
	
	public Object parse(String filename) throws ParserException, IOException{
		try {
			return parser.parse(new CUP2Scanner(new InputStreamReader(new FileInputStream(filename))));
		} catch (LRParserException e) {
			throw new ParserException("LRParserException: "+e.getMessage());
		}
	}
}
