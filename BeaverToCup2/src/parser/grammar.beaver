%import "lang.ast.*";
%package "lang.parser";

%terminals PACKAGE, CLASS, EMBED, TYPEOF, GOAL, TERMINALS, HEADER, LEFT, RIGHT, NONASSOC, IMPORT;
%terminals COLONBRACKETS, STRING, ID;
%terminals EQUALS, ALTERNATE, PERIOD, SEMICOLON, COMMA, AT;

%typeof beaverfile =				"BeaverFile";
%typeof linelist =					"List";
%typeof prodlist =					"List";
%typeof terminalslist =				"List";
%typeof tokenlist =					"List";

%typeof line =						"BeaverLine";
%typeof optprec =					"Opt";
%typeof optsemaction =				"Opt";
%typeof optSemactionVariableName =	"Opt";

%typeof ID =						"String";
%typeof COLONBRACKETS =				"String";
%typeof STRING =					"String";

beaverfile = linelist.a								{: return new BeaverFile(a); :};

linelist =
													{: return new List(); :}
	| linelist.a line.b								{: return a.add(b); :};

line =
	  CLASS STRING.a SEMICOLON						{: return new ClassDecl(new StringToken(a)); :}
	| PACKAGE STRING.a SEMICOLON					{: return new PackageDecl(new StringToken(a)); :}
	| IMPORT STRING.a SEMICOLON						{: return new ImportDecl(new StringToken(a)); :}
	
	| EMBED COLONBRACKETS.a SEMICOLON				{: return new EmbedDecl(new ColonBrackets(a)); :}
	| HEADER COLONBRACKETS.a SEMICOLON				{: return new HeaderDecl(new ColonBrackets(a)); :}
	
	| LEFT terminalslist.a SEMICOLON				{: return new Prec("left", a); :}
	| RIGHT terminalslist.a SEMICOLON				{: return new Prec("right", a); :}
	| NONASSOC terminalslist.a SEMICOLON			{: return new Prec("nonassoc", a); :}
	
	| GOAL ID.a SEMICOLON							{: return new GoalDecl(new Identifier(a)); :}
	
	| TYPEOF ID.a EQUALS STRING.b SEMICOLON			{: return new IntermTypeDecl(new Identifier(a), new StringToken(b)); :}
	| TERMINALS terminalslist.a SEMICOLON			{: return new TerminalsDecl(a); :}
	| ID.a EQUALS prodlist.b SEMICOLON				{: return new Prod(new Identifier(a), b); :};

terminalslist =
	  ID.a											{: return new List().add(new Identifier(a)); :}
	| terminalslist.a COMMA ID.b					{: return a.add(new Identifier(b)); :};

prodlist =
	  tokenlist.a optprec.prec optsemaction.b		{: return new List().add(new Rhs(a, b, prec)); :}
	| prodlist.l ALTERNATE tokenlist.a optprec.prec optsemaction.b	{: return l.add(new Rhs(a, b, prec)); :};

tokenlist =
													{: return new List(); :}
	| tokenlist.l ID.a optSemactionVariableName.b	{: return l.add(new TokenOrIntermediate(new Identifier(a), b)); :};

optprec =
													{: return new Opt(); :}
	| AT ID.a										{: return new Opt(new Identifier(a)); :};

optSemactionVariableName =
													{: return new Opt(); :}
	| PERIOD ID.a									{: return new Opt(new Identifier(a)); :};

optsemaction =
													{: return new Opt(); :}
	| COLONBRACKETS.a								{: return new Opt(new SemAction(new ColonBrackets(a))); :};
