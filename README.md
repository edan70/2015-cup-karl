BeaverToCup2
========

This is the repository of BeaverToCup2, a translator tool from .beaver files to CUP2Specefications, aiding in migrating from using the Beaver parser to using the CUP2 parser.

BUILDING
--------

To build the project:

ant build-all

This generates "BeaverToCup2.jar". Alternatively use launch option build-all in Eclipse.

TESTING
--------
To build the project using first Beaver and then Cup2:

ant test

This should pass all tests. However, the ultimate test is to test ExtendJ with CUP2, using this project. See advanced testing.

RUNNING
--------

To run it use (for example):

java -jar BeaverToCup2.jar inFile=somefile.beaver

Optional parameters, with default values/possible values where applicable:

  * outDir=. (where output CUP2Spec.java will be placed)
  * defaultTokenType=beaver.Symbol (used as type if not declared in .beaver file)
  * parserClass=lang.parser.BeaverParser | lang.parser.CUP2Parser (which parser to use when translating)
  * extendj (runs a small preprocessor that does scripted edits to extendj .beaver file before processing, removes header and embed sections etc)

DETAILED BUILD PROCESS
--------

This explains what happens in the build process. You should not need to run any of these ant tasks manually.

  * clean : delete everything in ant-bin and src/gen.
  * beaver : this builds the beaver parser and beaver compatible scanner
  * jar : this first jar can only use beaver.
  * cup2 : this uses the first jar generated above to translate itself to cup2
  * jar : this second jar can use beaver and cup2.

BUILDING EXTENDJ WITH CUP2 SUPPORT
--------

  1. Download https://bitbucket.org/extendj/extendj
  2. Patch extendj with cup2 support by extracting extendj-cup2.zip from this repo into extendj root.
  3. Build extendj with cup2 support: ant java8, ant cup2, ant build, ant jar

This sets CUP2 as the default parser. If you want to be able to switch between parsers, just change the field ParserFactory.which from "JavaParserCUP2" to "JavaParserBeaver".

RUNNING EXTENDJ WITH CUP2 SUPPORT
--------

Run:

java -jar extendj.jar -cup2 File.java

Actually, in the patch, CUP2 is the default so -cup2 is not needed.

ADVANCED TESTING
--------
The tests provided are very basic. To extensively test BeaverToCup2:

  1. Build extendj with CUP2 support according to above.
  2. Download https://bitbucket.org/extendj/regression-tests/
  3. Place finished extendj.jar in regression-tests root.
  4. Run regression-tests: ant java8